from selenium import webdriver
from selenium.webdriver.chrome.options import Options
driver = webdriver.Chrome(executable_path ="chromedriver.exe")
driver.get('https://google.com')
JS='''
function check_div(e){
if(e.tagName==='DIV'){
        var TEXT=[].reduce.call(e.childNodes, function(a, b) { return a + (b.nodeType === 3 ? b.textContent : ''); }, '');
          }
          else{
            var TEXT=e.textContent;
          }
return TEXT;

}
function isHidden(el) {
    return (el.offsetParent === null)
}
function getPathTo(element) {
    if (element.id!=='')
        return "//*[@id='"+element.id+"']";
    
    
    if (element===document.body)
    
        return element.tagName.toLowerCase();

    var ix= 0;
    var siblings= element.parentNode.childNodes;
    for (var i= 0; i<siblings.length; i++) {
        var sibling= siblings[i];
        
        if (sibling===element){
        
        if(getPathTo(element.parentNode)==='body'){
            return 'html/'+getPathTo(element.parentNode) + '/' + element.tagName.toLowerCase() + '[' + (ix + 1) + ']';
        }
        else{
        return getPathTo(element.parentNode) + '/' + element.tagName.toLowerCase() + '[' + (ix + 1) + ']';

        }

        }
        
        if (sibling.nodeType===1 && sibling.tagName === element.tagName) {
            ix++;
        }
    }
}

function getPageXY(element) {
    var x= 0, y= 0;
    while (element) {
        x+= element.offsetLeft;
        y+= element.offsetTop;
        element= element.offsetParent;
    }
    return [x, y];
}
function getElementsByXPath(xpath, parent)
{
    let results = [];
    let query = document.evaluate(xpath, parent || document,
        null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
    for (let i = 0, length = query.snapshotLength; i < length; ++i) {
        results.push(query.snapshotItem(i));
    }
    return results;
}
var results='False';
var ALL=[];
X=''
var guessable_elements = ['input','button','p','h1','h2','h3','h4','h5','h6','label','a','div','tr','td','th','span','option','marquee','li','blockquote','address','q'];
var attribute_list = ['id','name','placeholder','value','title','type','class','src'];
var A=document.body.getElementsByTagName("*");
for (g=0;g<guessable_elements.length; g++){
    
    var elements=document.body.getElementsByTagName(guessable_elements[g]);
    
    for (e=0;e<elements.length;e++){
        
        if(!elements[e].hasAttribute("type")||(elements[e].hasAttribute("type")&&elements[e].type!=="hidden"&&elements[e].type!=="HIDDEN")&&elements[e]!==null){
        for (attr=0;attr<attribute_list.length;attr++){

            if(elements[e].hasAttribute(attribute_list[attr])){



            ///console.log(attribute_list[attr])
            var X=guess_xpath(guessable_elements[g],attribute_list[attr],elements[e])
            //console.log(X);
            var locator=getElementsByXPath(X);
            if (locator.length===1){
            ///console.log(locator[0].tagName)
            
            var TEXT=check_div(elements[e]);
            
            if(TEXT.trim()!==""&&!isHidden(locator[0])){
            var results='True';
            break;
            }
            }
            ///console.log( getElementsByXPath(X) );

            }

            }


        if (results=='True'&&X!==''){
        
        ALL.push(X);
        X=''
        results='False'
        


        }
        else{
        ///console.log(elements[e]);
        var TEXT=check_div(elements[e]);
        if(TEXT.trim()!==""&&!isHidden(elements[e])){
        
        X=getPathTo(elements[e])
        
        
            
            
        ///console.log("ZX",X)
        locator=getElementsByXPath(X)
        
        if(locator.length===1){
        console.log(locator[0])
        
        ALL.push(X)
        X=''
        results='False'
        }
        }


        }

        }


    }
    
    }
function guess_xpath(tag,attr,element){

if(attr==='class'){

var attr2='className';

}
else{

var attr2=attr;
}

if(Array.isArray(element[attr2])){

for(i=0;i<element[attr2].length;i++){

 element[attr2].push(element[attr2][i])
 

}

element[attr2]=element[attr2].join(" ");



}



var XPATH='//'+tag+'[@'+attr+'="'+element[attr2]+'"]'
return XPATH;
}


return ALL;

'''

A=driver.execute_script(JS)
for a in A:
    e=(driver.find_element_by_xpath(a).text)
    print(e,a)
#driver.quit()
