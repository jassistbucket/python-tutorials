#Spacy -NLP Calculator
from word2number import w2n
import spacy
import en_core_web_sm
nlp=en_core_web_sm.load()
doc=nlp("what is the value of two plus two?")
FLAG=0
Result=1
for token in doc:
    #print(token.text,token.lemma_)
    if token.lemma_=="multiply":
        FLAG=1
if FLAG==1:
    #print("Ready for multiplication")
    for ent in doc.ents:
        #print(ent.text,ent.label_)
        if ent.label_=="CARDINAL":
            #print(w2n.word_to_num(ent.text))
            Result*=w2n.word_to_num(ent.text)
    print("Answer: ",Result)
else:
    print("Sorry I can multiply only :(")
